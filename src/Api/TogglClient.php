<?php

namespace Comdatia\Toggl\Api;

use Comdatia\Toggl\AbstractClient;
use Comdatia\Toggl\Exception\InvalidClassException;
use Comdatia\Toggl\Model\AbstractRemoteModel;
use Comdatia\Toggl\Model\Client;
use Comdatia\Toggl\Model\User;
use Comdatia\Toggl\Model\Workspace;

class TogglClient extends AbstractClient
{
    const API_BASE_PATH = '/api/v8/';

    public function me()
    {
        return new User($this, $this->get('me'));
    }

    /**
     *  =====================================
     *  Clients Functions
     *  =====================================.
     */
    public function clients()
    {
        $data = $this->get('clients');
        $items = [];
        foreach ($data as $item) {
            $items[] = new Client($this, $item);
        }

        return $items;
    }

    public function workspaces()
    {
        $data = $this->get('workspaces');
        $items = [];
        foreach ($data as $item) {
            $items[] = new Workspace($this, $item);
        }

        return $items;
    }

    /**
     *  =====================================
     *  Utility Functions
     *  =====================================.
     */
    public function make($modelClass)
    {
        if (is_subclass_of($modelClass, AbstractRemoteModel::class)) {
            return new $modelClass($this);
        } else {
            throw new InvalidClassException($modelClass, AbstractRemoteModel::class);
        }
    }

    public function request($method, $url, $options = [])
    {
        return parent::request($method, self::API_BASE_PATH.$url, $options);
    }
}
