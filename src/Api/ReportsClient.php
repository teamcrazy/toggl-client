<?php

namespace Comdatia\Toggl\Api;

use Comdatia\Toggl\AbstractClient;
use Comdatia\Toggl\Exception\InvalidClassException;
use Comdatia\Toggl\Model\AbstractRemoteModel;
use Comdatia\Toggl\Model\TimeEntry;
use Comdatia\Toggl\Model\Workspace;
use GuzzleHttp\Client;

class ReportsClient extends AbstractClient
{
    const API_BASE_PATH = '/reports/api/v2/';
    protected $togglClient;

    public function __construct($togglToken, Client $guzzleClient = null)
    {
        $this->togglClient = new TogglClient($togglToken, $guzzleClient);
        parent::__construct($togglToken, $guzzleClient);
    }

    public function searchTimesheets(Workspace $workspace, $params)
    {
        $uri = 'details?user_agent='.urlencode(static::USER_AGENT).'&';
        foreach ($params as $key => $value) {
            $uri .= $key.'=';
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            $uri .= urlencode($value).'&';
        }
        $uri .= 'workspace_id='.$workspace->id.'&';
        $data = $this->getPaginatedRequest($uri, 'Comdatia\Toggl\Model\TimeEntry', TimeEntry::class);

        return $data;
    }

    public function request($method, $url, $options = [])
    {
        return parent::request($method, self::API_BASE_PATH.$url, $options);
    }

    protected function getPaginatedRequest($uri, $modelClass = null)
    {
        $response = $this->get($uri);
        $totalData = [];
        $this->addRemoteData($totalData, $response->data, $modelClass);
        $totalItems = $response->total_count;
        $pageSize = $response->per_page;

        $currentPage = 2;
        while (count($totalData) < $totalItems) {
            $response = $this->get($uri.'page='.$currentPage);
            $this->addRemoteData($totalData, $response->data, $modelClass);
            $currentPage++;
        }

        return $totalData;
    }

    protected function addRemoteData(&$toArray, $dataArray, $modelClass = null)
    {
        foreach ($dataArray as $item) {
            if ($modelClass) {
                $item = new $modelClass($this->togglClient, $item);
                $toArray[] = $item;
            }
        }
    }
}
