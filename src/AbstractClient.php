<?php

namespace Comdatia\Toggl;

use Comdatia\Toggl\Exception\AuthenticationException;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class AbstractClient
{
    protected $guzzleClient;
    protected $togglToken;

    /** DateTime $lastRequest */
    protected $lastRequest = null;

    const TOGGL_BASE_URL = 'https://api.track.toggl.com';
    const TOGGL_AUTHFAIL_STATUS = 403;
    const USER_AGENT = 'Comdatia Toggl Client';

    /**
     * Toggl token isn't validated at construct, but persisted for future, stateless requests.
     *
     * Authentication with username/password basic auth isn't supported to discourage
     * the use of this method.
     */
    public function __construct($togglToken, Client $guzzleClient = null)
    {
        $this->togglToken = $togglToken;
        if ($guzzleClient === null) {
            $this->guzzleClient = new Client([
                'auth' => [$togglToken, 'api_token'],
            ]);
        } else {
            $this->guzzleClient = $guzzleClient;
        }
    }

    public function request($method, $url, $options = [])
    {
        $this->rateLimit();
        try {
            $result = $this->guzzleClient->request($method, self::TOGGL_BASE_URL.$url, $options);
        } catch (\GuzzleHttp\Exception\ClientException $clientException) {
            if ($clientException->getResponse()->getStatusCode() == static::TOGGL_AUTHFAIL_STATUS) {
                throw new AuthenticationException();
            } else {
                throw $clientException;
            }
        }

        return json_decode($result->getBody()->getContents());
    }

    public function get($url)
    {
        return $this->request('GET', $url);
    }

    public function put($url, $data)
    {
        return $this->request('PUT', $url, [RequestOptions::JSON => $data]);
    }

    public function post($url, $data)
    {
        return $this->request('POST', $url, [RequestOptions::JSON => $data]);
    }

    public function delete($url)
    {
        return $this->request('DELETE', $url);
    }

    /**
     * Ensure that the request isn't sent too soon after
     * the previous one.  Toggl docs suggest requests should be no closer than 1
     * second apart.
     */
    protected function rateLimit()
    {
        if ($this->lastRequest) {
            $lastRequest = ($this->lastRequest->getTimestamp() * 1000) + ($this->lastRequest->format('u') / 1000);
            $now = new DateTime();
            $now = ($now->getTimestamp() * 1000) + ($now->format('u') / 1000);
            $diff = $now - $lastRequest;
            if ($diff < 1000) {
                $milisecs = 1000 - $diff;
                usleep($milisecs * 1000);
            }
            $this->lastRequest = new DateTime();
        }
    }
}
