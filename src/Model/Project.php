<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\Exception\InvalidClassException;
use Comdatia\Toggl\Exception\InvalidRemoteModelException;

class Project extends AbstractRemoteModel
{
    protected $clientObject;
    protected $workspaceObject;

    protected $attributes = [
        'name',
        'wid',
        'cid',
        'active',
        'is_private',
        'template',
        'template_id',
        'billable',
        'auto_estimates',
        'estimated_hours',
        'color',
        'rate',
    ];
    protected $createUrl = 'projects';
    protected $updateUrl = 'projects/%s';
    protected $destroyUrl = 'projects/%s';
    protected $refreshUrl = 'projects/%s';

    public function mockData()
    {
        return [
            'data' => [
                'id'=>193838628,
                'wid'=>777,
                'cid'=>123397,
                'name'=>'An awesome project',
                'billable'=>false,
                'is_private'=>true,
                'active'=>true,
                'at'=>'2013-03-06T12:15:37+00:00',
                'template_id'=>10237,
                'color'=> '5',
            ],
        ];
    }

    protected function getClient()
    {
        if ($this->clientObject === null && $this->data->cid) {
            $this->clientObject = new Client($this->parentClient);
            $this->clientObject->id = $this->data->cid;
            $this->clientObject->refresh();
        }

        return $this->clientObject;
    }

    protected function setClient($value)
    {
        if (! is_a($value, Client::class)) {
            throw new InvalidClassException($value, Client::class);
        }

        if (! $value->id) {
            throw new InvalidRemoteModelException($value);
        }
        $this->data->cid = $value->id;
        $this->clientObject = $value;
    }

    protected function getWorkspace()
    {
        if ($this->workspaceObject === null && $this->data->wid) {
            $this->workspaceObject = new Workspace($this->parentClient);
            $this->workspaceObject->id = $this->data->wid;
            $this->workspaceObject->refresh();
        }

        return $this->workspaceObject;
    }

    protected function setWorkspace($value)
    {
        if (! is_a($value, Workspace::class)) {
            throw new InvalidClassException($value, Workspace::class);
        }

        if (! $value->id) {
            throw new InvalidRemoteModelException($value);
        }
        $this->data->wid = $value->id;
        $this->workspaceObject = $value;
    }
}
