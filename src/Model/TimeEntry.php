<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\Exception\ReadOnlyPropertyException;

class TimeEntry extends AbstractRemoteModel
{
    protected $attributes = [
        'description',
        'wid',
        'pid',
        'tid',
        'billable',
        'start',
        'stop',
        'created_with',
        'tags',
    ];

    protected $createUrl = 'time_entries';
    protected $updateUrl = 'time_entries/%s';
    protected $destroyUrl = 'time_entries/%s';
    protected $refreshUrl = 'time_entries/%s';

    protected function getIsRunning()
    {
        return is_null($this->stop);
    }

    protected function setIsRunning($value)
    {
        throw new ReadOnlyPropertyException('is_running');
    }

    public function mockData()
    {
        return [
            'data' => [
                'id' => 436694100,
                'wid' => 777,
                'pid' => 193791,
                'tid' => 13350500,
                'billable' => false,
                'start' => '2013-02-27T01:24:00+00:00',
                'stop' => '2013-02-27T07:24:00+00:00',
                'duration' => 21600,
                'description' => 'Some serious work',
                'tags' => ['billed'],
                'at' => '2013-02-27T13:49:18+00:00',
            ],
        ];
    }

    protected function getBaseName()
    {
        return 'time_entry';
    }
}
