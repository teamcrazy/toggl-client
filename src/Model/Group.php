<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\Exception\InvalidClassException;
use Comdatia\Toggl\Exception\InvalidRemoteModelException;

class Group extends AbstractRemoteModel
{
    protected $projectsCollection = [];
    protected $attributes = [
        'wid',
        'name',
    ];
    protected $createUrl = 'groups';
    protected $updateUrl = 'groups/%s';
    protected $destroyUrl = 'groups/%s';
    protected $refreshUrl = 'groups/%s';
    protected $workspaceObject;

    public function mockData()
    {
        return [
            'data' => [
                'id' => 1239455,
                'wid' => 777,
                'name' => 'Developers',
                'at' => '2013-02-26T08:45:28+00:00',
            ],
        ];
    }

    protected function getWorkspace()
    {
        if ($this->workspaceObject === null && $this->data->wid) {
            $this->workspaceObject = new self($this->parentClient);
            $this->workspaceObject->id = $this->data->wid;
            $this->workspaceObject->refresh();
        }

        return $this->workspaceObject;
    }

    protected function setWorkspace($value)
    {
        if (! is_a($value, Workspace::class)) {
            throw new InvalidClassException(get_class($value), Workspace::class);
        }

        if (! $value->id) {
            throw new InvalidRemoteModelException($value);
        }
        $this->data->wid = $value->id;
        $this->workspaceObject = $value;
    }
}
