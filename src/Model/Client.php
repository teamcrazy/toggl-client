<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\Exception\InvalidClassException;
use Comdatia\Toggl\Exception\InvalidRemoteModelException;
use Comdatia\Toggl\Exception\ReadOnlyPropertyException;

class Client extends AbstractRemoteModel
{
    protected $projectsCollection = [];
    protected $attributes = ['name', 'wid', 'notes'];
    protected $createUrl = 'clients';
    protected $updateUrl = 'clients/%s';
    protected $destroyUrl = 'clients/%s';
    protected $refreshUrl = 'clients/%s';
    protected $workspaceObject;

    public function mockData()
    {
        return [
            'data' => [
                'id'=>1239455,
                'wid'=>777,
                'name'=>'Very Big Company',
                'at'=>'2013-02-26T08:45:28+00:00',
                'notes'=> 'Contact: John Jacob Jingleheimer Schmidt',
            ],
        ];
    }

    protected function getProjects()
    {
        $remoteProjects = $this->get("clients/{$this->id}/projects");
        $projects = [];
        foreach ($remoteProjects as $project) {
            $projects[] = new Project($this->parentClient, $project);
        }

        return $projects;
    }

    protected function setProjects()
    {
        throw new ReadOnlyPropertyException('projects');
    }

    protected function getWorkspace()
    {
        if ($this->workspaceObject === null && $this->data->wid) {
            $this->workspaceObject = new Workspace($this->parentClient);
            $this->workspaceObject->id = $this->data->wid;
            $this->workspaceObject->refresh();
        }

        return $this->workspaceObject;
    }

    protected function setWorkspace($value)
    {
        if (! is_a($value, Workspace::class)) {
            throw new InvalidClassException($value, Workspace::class);
        }

        if (! $value->id) {
            throw new InvalidRemoteModelException($value);
        }
        $this->data->wid = $value->id;
        $this->workspaceObject = $value;
    }
}
