<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\Exception\ReadOnlyPropertyException;

class Workspace extends AbstractRemoteModel
{
    protected $projectsCollection = [];
    protected $attributes = [
        'name',
        'premium',
        'admin',
        'logo_url',
    ];
    protected $createUrl = 'workspaces';
    protected $updateUrl = 'workspaces/%s';
    protected $destroyUrl = 'workspaces/%s';
    protected $refreshUrl = 'workspaces/%s';

    public function mockData()
    {
        return [
            'data' => [
                'id'=>3134975,
                'name'=>"John's personal ws",
                'premium'=>true,
                'admin'=>true,
                'logo_url'=>'my_logo.png',
            ],
        ];
    }

    protected function getGroups()
    {
        $groupData = $this->parentClient->get('workspaces/'.$this->id.'/groups');
        $items = [];
        if ($groupData !== null) {
            foreach ($groupData as $data) {
                $items[] = new Group($this->parentClient, $data);
            }
        }

        return $items;
    }

    protected function setGroups($value)
    {
        throw new ReadOnlyPropertyException('groups');
    }

    protected function getMembers()
    {
        $memberData = $this->parentClient->get('workspaces/'.$this->id.'/users');
        $items = [];
        if ($memberData !== null) {
            foreach ($memberData as $user) {
                $items[] = new User($this->parentClient, $user);
            }
        }

        return $items;
    }

    protected function setMembers($value)
    {
        throw new ReadOnlyPropertyException('members');
    }

    protected function getClients()
    {
        $clients = $this->parentClient->get('workspaces/'.$this->id.'/clients');
        $items = [];
        if ($clients !== null) {
            foreach ($clients as $client) {
                $items[] = new Client($this->parentClient, $client);
            }
        }

        return $items;
    }

    protected function setClients($value)
    {
        throw new ReadOnlyPropertyException('clients');
    }

    protected function getTags()
    {
        $clients = $this->parentClient->get('workspaces/'.$this->id.'/tags');
        $items = [];
        if ($clients !== null) {
            foreach ($clients as $client) {
                $items[] = new Tag($this->parentClient, $client);
            }
        }

        return $items;
    }

    protected function setTags($value)
    {
        throw new ReadOnlyPropertyException('tags');
    }
}
