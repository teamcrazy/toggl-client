<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\AbstractClient;

abstract class AbstractRemoteModel
{
    protected $data = null;
    protected $parentClient = null;
    protected $attributes = [];
    protected $refreshUrl;
    protected $createUrl;
    protected $updateUrl;
    protected $destroyUrl;

    public function persist()
    {
        $id = $this->id;
        $payload = $this->getPayload();

        if ($id) {
            $result = $this->put(sprintf($this->updateUrl, (string) $id), [$this->getBaseName() => $payload]);
            $this->data = $result->data;
        } else {
            $result = $this->post($this->createUrl, [$this->getBaseName() => $payload]);
            $this->data = $result->data;
        }

        return true;
    }

    public function __debugInfo()
    {
        return [
            'refreshUrl' => $this->refreshUrl,
            'createUrl' => $this->createUrl,
            'updateUrl' => $this->updateUrl,
            'destroyUrl' => $this->destroyUrl,
            'attributes' => $this->attributes,
            'data' => $this->data,
        ];
    }

    public function __construct(AbstractClient $parentClient, $data = null)
    {
        $this->parentClient = $parentClient;
        $this->data = $data ? $data : new \stdClass();
    }

    public function __get($property)
    {
        $funcName = $this->getPropertyFunctionName($property);
        if (method_exists($this, 'get'.$funcName)) {
            return $this->{'get'.$funcName}();
        } else {
            return property_exists($this->data, $property) ? $this->data->{$property} : null;
        }
    }

    public function __set($property, $value)
    {
        $funcName = $this->getPropertyFunctionName($property);
        if (method_exists($this, 'set'.$funcName)) {
            $this->{'set'.$funcName}($value);
        } else {
            $this->data->{$property} = $value;
        }
    }

    public function refresh()
    {
        $result = $this->get(sprintf($this->refreshUrl, (string) $this->id));
        $this->data = $result->data;
    }

    public function destroy()
    {
        $this->delete(sprintf($this->destroyUrl, (string) $this->id));
        $this->id = null;
    }

    protected function get($url)
    {
        return $this->parentClient->get($url);
    }

    protected function put($url, $data)
    {
        return $this->parentClient->put($url, $data);
    }

    protected function post($url, $data)
    {
        return $this->parentClient->post($url, $data);
    }

    protected function delete($url)
    {
        return $this->parentClient->delete($url);
    }

    protected function getPropertyFunctionName($property)
    {
        return str_replace('_', '', ucwords($property, '_'));
    }

    protected function getPayload()
    {
        $data = [];
        foreach ($this->attributes as $attribute) {
            $data[$attribute] = $this->{$attribute};
        }

        return $data;
    }

    protected function getBaseName()
    {
        $path = explode('\\', static::class);

        return strtolower(array_pop($path));
    }

    abstract public function mockData();
}
