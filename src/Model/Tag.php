<?php

namespace Comdatia\Toggl\Model;

use Comdatia\Toggl\Exception\InvalidClassException;
use Comdatia\Toggl\Exception\InvalidRemoteModelException;

class Tag extends AbstractRemoteModel
{
    protected $attributes = ['name', 'wid'];
    protected $createUrl = 'tags';
    protected $updateUrl = 'tags/%s';
    protected $destroyUrl = 'tags/%s';
    protected $refreshUrl = 'tags/%s';
    protected $workspaceObject;

    public function mockData()
    {
        return [
            'data' => [
                'id' => 1239455,
                'wid' => 777,
                'name' => 'not billed',
            ],
        ];
    }

    protected function getWorkspace()
    {
        if ($this->workspaceObject === null && $this->data->wid) {
            $this->workspaceObject = new Workspace($this->parentClient);
            $this->workspaceObject->id = $this->data->wid;
            $this->workspaceObject->refresh();
        }

        return $this->workspaceObject;
    }

    protected function setWorkspace($value)
    {
        if (! is_a($value, Workspace::class)) {
            throw new InvalidClassException($value, Workspace::class);
        }

        if (! $value->id) {
            throw new InvalidRemoteModelException($value);
        }
        $this->data->wid = $value->id;
        $this->workspaceObject = $value;
    }
}
