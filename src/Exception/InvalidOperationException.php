<?php

namespace Comdatia\Toggl\Exception;

class InvalidOperationException extends \Exception
{
    public $model;
    public $operation;

    public function __construct($model, $operation)
    {
        $this->model = $model;
        $this->operation = $operation;
        parent::__construct($operation.' invalid for '.get_class($model));
    }
}
