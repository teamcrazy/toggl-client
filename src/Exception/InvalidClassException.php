<?php

namespace Comdatia\Toggl\Exception;

use Exception;

class InvalidClassException extends Exception
{
    public function __construct($model, $expectedParent)
    {
        parent::__construct($model.' is not a subclass of '.$expectedParent);
    }
}
