<?php

namespace Comdatia\Toggl\Exception;

class ReadOnlyPropertyException
{
    public function __construct($property)
    {
        parent::__construct($property.' is a read only property');
    }
}
