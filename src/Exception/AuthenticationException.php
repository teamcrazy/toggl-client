<?php

namespace Comdatia\Toggl\Exception;

use Exception;

class AuthenticationException extends Exception
{
    public function __construct()
    {
        parent::__construct('Invalid Toggl token');
    }
}
