<?php

namespace Comdatia\Toggl\Exception;

class InvalidRemoteModelException extends \Exception
{
    public $remoteModel;

    public function __construct($remoteModel)
    {
        $this->remoteModel = $remoteModel;

        parent::__construct('The given model does not have a valid remote id');
    }
}
