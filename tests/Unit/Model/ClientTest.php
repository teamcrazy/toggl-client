<?php

use Comdatia\Toggl\Api\TogglClient;
use Comdatia\Toggl\Model\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

final class ClientTest extends TestCase
{
    public function testRefresh()
    {
        $mockData = Client::mockData();
        $mock = new MockHandler([
            new Response('200', [], json_encode($mockData)),
            new Response('200', [], json_encode($mockData)),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $guzzleClient = new GuzzleClient(['handler' => $handlerStack]);

        $togglClient = new TogglClient('token', $guzzleClient);
        $client = $togglClient->make(Comdatia\Toggl\Model\Client::class);
        $client->id = $mockData['data']['id'];
        $client->refresh();
        $this->assertEquals($mockData['data']['name'], $client->name);
        $client->refresh();
        $this->assertEquals($mockData['data']['name'], $client->name);
    }
}
