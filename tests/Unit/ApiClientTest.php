<?php

use Comdatia\Toggl\AbstractClient;
use Comdatia\Toggl\Api\TogglClient;
use Comdatia\Toggl\Exception\AuthenticationException;
use Comdatia\Toggl\Model\Client;
use Comdatia\Toggl\Model\User;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

final class ApiClientTest extends TestCase
{
    public function testInvalidToken()
    {
        $this->expectException(AuthenticationException::class);
        $mockData = User::mockData();
        $mock = new MockHandler([
            new Response(TogglClient::TOGGL_AUTHFAIL_STATUS),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $guzzleClient = new GuzzleClient(['handler' => $handlerStack]);

        $togglClient = new TogglClient('token', $guzzleClient);
        $togglClient->me();
    }

    public function testValidToken()
    {
        $mockData = User::mockData();
        $mock = new MockHandler([
            new Response('200', [], json_encode($mockData)),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $guzzleClient = new GuzzleClient(['handler' => $handlerStack]);

        $togglClient = new TogglClient('token', $guzzleClient);
        $result = $togglClient->me();
        $this->assertInstanceOf(User::class, $result);
    }

    public function testClients()
    {
        $mockData = [
            [
                'id' => 1239455,
                'wid'=> 777,
                'name'=> 'Very Big Company',
                'notes'=> 'something about the client',
                'at'=> '2013-02-26T08:55:28+00:00',
            ], [
                'id'=> 1239456,
                'wid'=> 777,
                'name'=> 'Small Startup',
                'notes'=> 'Really cool people',
                'at'=> '2013-03-26T08:55:28+00:00',
            ],
        ];
        $mock = new MockHandler([
            new Response('200', [], json_encode($mockData)),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $guzzleClient = new GuzzleClient(['handler' => $handlerStack]);

        $togglClient = new TogglClient('token', $guzzleClient);
        $clients = $togglClient->clients();
        $this->assertCount(2, $clients);
        $this->assertEquals($mockData['0']['name'], $clients[0]->name);
    }
}
